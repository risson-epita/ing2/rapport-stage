\subsubsection{Déroulement concret de l'étude}

\paragraph{Déploiement du cluster de test}

Deux méthodes de déploiement sont recommandées par la documentation de Ceph~:

\begin{description}
    \item[Cephadm] nouvelle méthode d'installation de Ceph via des conteneurs
        docker ou podman, lancés par des services systemd~;
    \item[Rook] Ceph déployé via Kubernetes, ou pour intégrer un cluster
        Kubernetes dans un cluster Ceph existant.
\end{description}

D'autres méthodes d'installation de Ceph sont également listées dans la
documentation, dont la plus populaire \texttt{ceph-ansible}, qui déploie Ceph
avec un outil de provisioning software, Ansible.

Il a été choisi d'utiliser Cephadm pour déployer le cluster de test, et
subséquemment le cluster de production, pour les raisons suivantes~:

\begin{itemize}
    \item c'est une méthode de déploiement recommandée, contrairement à
        \texttt{ceph-ansible}~;
    \item elle ne nécessite pas de déployer un cluster Kubernetes, qui est un
        outil lourd à gérer~;
    \item elle est testée sur la distribution Linux utilisée par le CRI pour ses
        serveurs (Ubuntu)~;
    \item elle supporte les nouvelles fonctionnalités de la CLI Ceph~;
    \item elle installe un tableau de bord permettant de monitorer l'état du
        cluster.
\end{itemize}

\paragraph{Tests effectués}

Une fois le cluster déployé, certains tests ont été effectués, dont~:

\begin{itemize}
    \item création d'une pool pour chacune des méthodes d'accès aux données
        hébergées par Ceph~: RBD (block device), rados-gw (S3), CephFS (système
        de fichiers réseau), Ganesha (NFS)~;
    \item interactions spécifiques à chacune de ces méthodes, depuis un client
        n'appartenant pas au cluster, ce qui a impliqué la création de clients
        et de règles de gestion de permissions~;
\end{itemize}

En plus de ces tests relativement basiques destinés à remplir le cluster de
test de données approximativement représentatives d'un cluster de production, a
été testé la mise à jour du cluster. Voici la procédure suivie pour faire
celle-ci~:

\begin{enumerate}
    \item se documenter sur les changements apportés par la mise à jour,
        décrits dans les \textit{Release notes} de Ceph, notamment sur les
        changements cassants et sur les éventuelles opérations à effectuer
        manuellement~;
    \item vérifier que les sauvegardes existantes sont intactes, et dans le cas
        contraire les recréer~;
    \item Ceph étant un système distribué, il est possible de mettre à jour un
        n\oe{}ud après l'autre, et ainsi vérifier que les opérations se
        déroulent correctement tout au long de la mise à jour~;
    \item mettre à jour un n\oe{}ud tel que décrit dans la documentation de
        Ceph~;
    \item monitorer son état de fonctionnement, via le dashboard ou en ligne de
        commande avec \texttt{ceph status} et \texttt{ceph health}~;
    \item si la mise à jour se passe correctement, répéter ces étapes avec le
        n\oe{}ud suivant~;
    \item sinon, suivre la procédure de retour en arrière tel que décrite dans
        la documentation de Ceph~;
    \item si celle-ci échoue, et qu'un seul n\oe{}ud Ceph a été mis à jour, il
        est possible de réinstaller celui-ci, les données ne seront pas
        perdues, celles-ci étant répliquées trois fois~;
    \item sinon, il faut restaurer les données à partir des sauvegardes.
\end{enumerate}

\paragraph{Déploiement du cluster de production et benchmark}

Suite à la réception de 5 nouveaux serveurs, 5 cartes réseaux 10 Gbps et 10 SSD
NVMe 3.2 To par le service et à leur installation, il fût ensuite possible de
déployer un cluster Ceph sur ces machines qui étaient destinées à être utilisées
en production. Cependant, celles-ci n'ont pas tout de suite été utilisée en
production, car il a fallu benchmarker différentes configurations possibles. En
effet, Ceph permet de configurer certaines options affectant son fonctionnement
interne afin d'améliorer ses performances en fonction de la disposition du
déploiement. Les différentes configurations qui ont été testées sont les
suivantes~:

\begin{description}
    \item[configuration initiale] afin d'avoir un point de comparaison, un
        premier benchmark a été réalisé sans aucun changement de
        configuration~;
    \item[debug log off] Ceph est assez verbeux par défaut, et ainsi passe un
        certain temps par opération à enregistrer ce qu'il fait à des fins de
        débogue. Ainsi, éteindre cette fonctionnalité peut améliorer ses
        performances~;
    \item[cluster network] Ceph permet de définir un \texttt{cluster network}
        qui est un réseau séparé (physique ou non) qui lui est réservé et est
        utilisé pour la communication entre les n\oe{}uds~;
    \item[C-State no sleep] les C-States Power Saving Modes sont l'ensemble des
        modes dans lesquels un c\oe{}ur d'un CPU d'une machine peut se trouver.
        Ainsi, lorsque certains c\oe{}urs ne sont pas utilisés, le CPU va les
        mettre en veille pour économiser de l'énergie, voire les éteindre
        complètement. Lorsque la charge CPU augmente et que celui-ci a besoin
        de plus de c\oe{}urs pour traiter les opérations, il va réveiller ceux
        en mode dormant, ce qui prend un temps non négligeable. Ainsi,
        désactiver les C-States dans lesquels les c\oe{}urs sont endormis peut
        améliorer les performances~;
    \item[nombre de n\oe{}uds] ce test, ainsi que le suivant, a pour but
        principal de tenter de prédire comment le cluster Ceph doit évoluer~:
        doit on rajouter plus de serveurs, ou plus de disques~? Ainsi, nous
        avons testé une configuration similaire avec un cluster composé de 3,
        puis de 4 n\oe{}uds~;
    \item[nombre d'OSD\footnotemark par n\oe{}ud] de même, nous avons
        testé une configuration similaire avec 3, puis 4 OSD par n\oe{}ud~;
    \item[nombre d'OSD par disque] Ceph permet de créer plusieurs OSD par
        disque, ainsi, lorsque le CPU est plus puissant que le nombre de
        disques disponibles dans la machine, il est possible de créer deux OSD
        par disque, et de ce fait améliorer la performance.
\end{description}
\footnotetext{Un OSD est un disque tel que vu par Ceph, ainsi qu'un démon
chargé de géré celui-ci}

Après ces tests, il a été possible de choisir la configuration optimale pour le
déploiement du cluster en production, et finalement la migration de l'ancien
cluster vers celui-ci.
