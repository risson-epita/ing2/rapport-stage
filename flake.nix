{
  # name = "ing2-rapport-stage"
  description = "ING2 Internship report";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils }:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem defaultSystems;
      inherit (lib) recursiveUpdate;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });

      version = "0.0.${lib.substring 0 8 (self.lastModifiedDate or self.lastModified or "19700101")}_${self.shortRev or "dirty"}";
    in
    recursiveUpdate
    {
      overlay = final: prev: {
        ing2-rapport-stage = final.stdenvNoCC.mkDerivation {
          pname = "ing2-rapport-stage";
          inherit version;

          src = self;

          buildInputs = with final; [
            (texlive.combine {
              inherit (texlive)
              scheme-medium
              appendix
              biblatex
              import
              placeins
              titlesec;
            })
          ];

          buildPhase = ''
            pushd src
            pdflatex default.tex
            bibtex default
            pdflatex default.tex
            pdflatex default.tex
            popd

            pushd abstract
            pdflatex default.tex
            pdflatex default.tex
            pdflatex default.tex
            popd
          '';

          installPhase = ''
            install -Dm644 src/default.pdf $out/share/ing2-rapport-stage.pdf
            install -Dm644 abstract/default.pdf $out/share/ing2-stage-abstract.pdf
          '';

          meta = with final.stdenv.lib; {
            inherit (self) description;
            maintainers = with maintainers; [ risson ];
            license = licenses.mit;
            platforms = platforms.unix;
          };
        };
      };
    }
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            (texlive.combine {
              inherit (texlive)
              scheme-medium
              appendix
              import
              titlesec;
            })
          ];
        };

        packages = { inherit (pkgs) ing2-rapport-stage; };
        defaultPackage = self.packages.${system}.ing2-rapport-stage;
      }
    ));
}
